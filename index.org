#+TITLE: GNU Artanis - A fast web application framework for Scheme
#+AUTHOR: Artanis Dev Team
# #+SETUPFILE: theme-readtheorg.setup
#+OPTIONS: num:nil toc:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style.css"/>
#+KEYWORDS: Scheme programming language, Web, Framework, GNU, Guile, GNU Artanis, Artanis, Web Application Framework, WAF, Functional Programming, Web Development, Web Server, Web Framework, Web Development, Web Application
#+DESCRIPTION: The first product-level modern Web framework of Scheme programming language.

#   :PROPERTIES:
#   :CUSTOM_ID: home
#   :END:

#+BEGIN_EXPORT html
<p id="nav">
<a href="index.html" id="cursor">Home</a> |
<a href="scheme.html">Scheme Intro</a> |
<a href="blog.html">Blog</a> |
<a href="https://ftp.gnu.org/gnu/artanis/">Download</a> |
<a href="https://www.gnu.org/software/artanis/manual/">Document</a> |
<a href="https://gitlab.com/hardenedlinux/artanis">Repo</a>
</p>
#+END_EXPORT

#+ATTR_HTML: :style margin: auto; width: 280px
file:img/logo.svg

#+BEGIN_QUOTE
Sailing to /dev/null

That is no future for mediocre coder.

The hacker is one another's arm. Codes in the editors.

Those dying generations - at their song.
#+END_QUOTE


GNU Artanis is **the first product-level modern Web framework of Scheme programming language**.
It is designed and maintained to be robust, fast, and easy to use for professional web development.

GNU Artanis was Certificated as Awesome Project at [[http://lispinsummerprojects.org/awesome-projects][2013 Lisp in summer projects]]

* Usage
  :PROPERTIES:
  :CUSTOM_ID: usage
  :END:
#+BEGIN_SRC scheme
(use-modules (artanis artanis))
(init-server)
(get "/hello"(lambda () "hello world"))

;; run it
(run #:port 8080)
#+END_SRC

#+BEGIN_SRC sh
curl localhost:8080/hello
==> hello world
#+END_SRC

* Features
  :PROPERTIES:
  :CUSTOM_ID: features
  :END:
- GPLv3+ & LGPLv3+
- Very lightweight - easy to hack and learn for newbies.
- Support JSON/CSV/XML/SXML.
- Support for WebSockets.
- Good i18n support.
- A complete web-server implementation, including an error page handler.
- High concurrent async non-blocking server core based on delimited continuations.
- Supported databases (through guile-dbi): MySQL/SQLite/PostgreSQL.
- Nice and easy web cache control.
- Efficient HTML template parsing.
- Efficient static file downloading/uploading.

* Download
  :PROPERTIES:
  :CUSTOM_ID: download
  :END:
The latest version of GNU Artanis can be downloaded from

- GNU FTP Server: [[https://ftp.gnu.org/gnu/artanis/]]
- Gitlab Releases: [[https://gitlab.com/hardenedlinux/artanis/-/tags]]

* Documents
  :PROPERTIES:
  :CUSTOM_ID: docs
  :END:
Official GNU Artanis manual: [[https://www.gnu.org/software/artanis/manual/]]

* Source Code
  :PROPERTIES:
  :CUSTOM_ID: source-code
  :END:
GNU Artanis is a free & open source software dual licenced under both [[https://artanis.dev#][GPLv3+ & LGPLv3+]]. You can get involved in the development or obtain a copy of source code of artanis from the below locations.

- Savannah: https://savannah.gnu.org/projects/artanis
- Gitlab: [[https://gitlab.com/hardenedlinux/artanis]]

Officially, GNU Artanis is maintained on Savannah, and folks can discuss on mailing-list. But we also provide GitLab for modern people.

* FAQ
  :PROPERTIES:
  :CUSTOM_ID: faq
  :END:
** What is it?

GNU Artanis is a framework for web authoring - for instance, generating HTML pages dynamically. In other words, a WAF (Web Application Framework).

** Who wrote it?

- Original author and the current maintainer:
  - Mu Lei aka [[https://nalaginrut.com/index][NalaGinrut]], a Certified Scheme Nut - someone who is always willing to build website with Scheme programming language.

- No hacker is an island, there're many contributors spent their intellegence and time on GNU Artanis, here's the name list, and it's never end:
  [[https://gitlab.com/hardenedlinux/artanis/-/blob/master/THANKS?ref_type=heads][Thanks list]]

** Why write it?

More seriously, Artanis is written using GNU Guile, one of the best implementations of Scheme language.

One day, the folks at GNU were discussing what language they would write the GNU website in - and many chose Python. But I found that strange, because the official extension language of GNU is GNU Guile. And I wondered aloud - why not start a brand new project to provide a web framework written with GNU Guile? To which RMS said, "It's cool, I like this idea."

But at that time, it was just an idea without a plan.

Fortunately, a few months later, the Guile community held a hack-potluck to celebrate Guile2 turning two - which is a contest to write a cool program in a few weeks. And so, Artanis was born.

** History

+ February 2013 - Artanis born at the GNU Guile hack-potluck.

+ 2013 - Artanis submitted to "Lisp In Summer Projects" contest. Received "Certificated awesome project award" in 2014.

+ 1st January, 2015 - the first stable version Artanis-0.0.1 was released.

+ 19th January, 2015 - offers Artanis to FSF/GNU, and RMS inducts it as an official GNU project. Artanis becomes GNU Artanis.

+ 29 December 2021 - GNU Artanis donates to HardenedLinux community to connect with product environment more tightly.

** Community

Welcome to join us in [[mailto: artanis@gnu.org][artanis@gnu.org ]]and [[https://gitlab.com/hardenedlinux/artanis][GitLab]].

** Projects using Artanis

- [[https://gitlab.com/NalaGinrut/colt][Colt blog engine]]
- [[https://github.com/hardenedlinux/chiba][Chiba project - the framework for next generation of cloud computing]]
- [[https://codeberg.org/luis-felipe/guix-packages-website][GNU Guix packages website]]
- [[https://codeberg.org/jjba23/byggsteg][Byggsteg - CI/CD system written in Scheme]]
