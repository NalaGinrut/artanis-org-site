(require 'org)
(require 'org-element)

(defun generate-rss-feed ()
  "Generate an RSS feed from Org-mode files in a specific directory."
  (let ((rss-file "rss.xml")       ; Output RSS file
        (blog-title "GNU Artanis Blog")    ; Blog title
        (blog-link "https://artanis.dev/blog") ; Blog URL
        (entries "")              ; Placeholder for feed entries
        (org-files (directory-files-recursively "blog" "\\.org$"))) ; Replace with your path
    ;; Loop through each Org file to extract metadata
    (dolist (file org-files)
      (with-temp-buffer
        (insert-file-contents file)
        (let* ((org-data (org-element-parse-buffer)) ; Parse the Org buffer
               (title (or (org-element-map org-data 'keyword
                              (lambda (kw)
                                (when (string= (org-element-property :key kw) "TITLE")
                                  (org-element-property :value kw)))
                              nil t)
                          "No Title"))               ; Default title if none found
               (date (format-time-string "%a, %d %b %Y %T %z"
                                         (or (org-element-map org-data 'keyword
                                               (lambda (kw)
                                                 (when (string= (org-element-property :key kw) "DATE")
                                                   (org-time-string-to-time (org-element-property :value kw))))
                                               nil t)
                                             (nth 5 (file-attributes file))))) ; File creation date as fallback
               (link (concat blog-link "/" (file-name-base file) ".html"))) ; Construct post URL
          ;; Append entry to entries
          (setq entries (concat entries
                                (format "
<item>
  <title>%s</title>
  <link>%s</link>
  <pubDate>%s</pubDate>
</item>" title link date))))))
    ;; Write the RSS XML to a file
    (with-temp-buffer
      (insert (format "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
<rss version=\"2.0\">
  <channel>
    <title>%s</title>
    <link>%s</link>
    %s
  </channel>
</rss>" blog-title blog-link entries))
      (write-file rss-file))
    (message "RSS feed generated: %s" rss-file)))
