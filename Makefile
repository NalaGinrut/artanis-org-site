.PHONY: all clean feed

blog-org := $(shell find blog -name '*.org')
blog-html := $(blog-org:.org=.html)

#blog-htmls: blog-html

all: index.html \
	scheme.html \
	$(blog-html) \
	blog.html \
	feed

feed:
	emacs --batch -l gen-feed.el --eval="(generate-rss-feed)"

blog/%.html: blog/%.org
	emacs $< --batch -l ../.fontlock.el -f org-html-export-to-html --kill

%.html: %.org
	emacs $< --batch -l .fontlock.el -f org-html-export-to-html --kill


clean:
	rm -f *.html *~
