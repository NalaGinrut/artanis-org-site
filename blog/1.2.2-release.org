#+title: GNU Artanis 1.2.2 release notes
#+AUTHOR: Artanis Dev Team
# #+SETUPFILE: theme-readtheorg.setup
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style.css"/>
#+KEYWORDS: Scheme programming language, Web, Framework, GNU, Guile, GNU Artanis, Artanis, Web Application Framework, WAF, Functional Programming, Web Development, Web Server, Web Framework, Web Development, Web Application
#+DESCRIPTION: The first product-level modern Web framework of Scheme programming language.
#+OPTIONS: num:nil toc:nil
#+INCLUDE: "../nav.html" export html

We are pleased to announce the release of GNU Artanis 1.2.2. [[https://lists.gnu.org/archive/html/artanis/2025-01/msg00024.html][Here's the official announcement]].

GNU Artanis is a web application framework written in Guile Scheme. It is a simple and efficient tool for building web applications. It is designed to support the development of dynamic websites, web services, and web applications. It is a modern web framework that is easy to use and easy to extend.

* What's new?

** Plugin mechanism

We have brand new plugin mechanism since Artanis 1.2.2. You can now write your own plugin and load it into GNU Artanis. This is a powerful feature that allows you to extend GNU Artanis in a way that suits your needs.

Here's a tutorial on how to write a plugin: [[file:/blog/plugins-in-artanis.html][Plugins in GNU Artanis]].

** Bug fixes

- Fixed bugs in i18n.
- Support SXML in i18n
- Fix to detect locale domain correctly


* How to start?

Please read the [[https://www.gnu.org/software/artanis/manual/artanis.html#Installation][Installation section in the manual]].

Happy hacking!
