// Wait for the DOM to fully load
document.addEventListener("DOMContentLoaded", function () {
    const content = document.getElementById("content");

    setTimeout(() => {
        // Initial and target styles
        let currentStyles = {
            textColor: [0, 255, 0], // Initial text color (green)
            bgColor: [0, 0, 0, 0.5], // Initial background color (rgba)
            fontSize: 16, // Initial font size
        };

        const targetStyles = {
            textColor: [97, 237, 129], // Target text color (#61ed81)
            bgColor: [19, 32, 27, 0.87], // Target background color (rgba)
            fontSize: 30, // Target font size in pixels
        };

        const increment = {
            textColor: [0.8, -0.8, 0.8], // Increments for R, G, B
            bgColor: [0.8, 0.8, 0.8, 0.1], // Increments for R, G, B, Alpha
            fontSize: 1, // Increment for font size
        };

        const interval = setInterval(() => {
            // Gradually adjust text color
            for (let i = 0; i < 3; i++) {
                if (currentStyles.textColor[i] !== targetStyles.textColor[i]) {
                    currentStyles.textColor[i] += increment.textColor[i];
                    currentStyles.textColor[i] = Math.min(
                        Math.max(currentStyles.textColor[i], 0),
                        targetStyles.textColor[i]
                    );
                }
            }

            // Gradually adjust background color
            for (let i = 0; i < 4; i++) {
                if (currentStyles.bgColor[i] !== targetStyles.bgColor[i]) {
                    currentStyles.bgColor[i] += increment.bgColor[i];
                    currentStyles.bgColor[i] = Math.min(
                        Math.max(currentStyles.bgColor[i], 0),
                        targetStyles.bgColor[i]
                    );
                }
            }

            // Gradually adjust font size
            if (currentStyles.fontSize < targetStyles.fontSize) {
                currentStyles.fontSize += increment.fontSize;
                currentStyles.fontSize = Math.min(
                    currentStyles.fontSize,
                    targetStyles.fontSize
                );
            }

            // Apply the updated styles
            content.style.color = `rgb(${Math.round(currentStyles.textColor[0])}, ${Math.round(currentStyles.textColor[1])}, ${Math.round(currentStyles.textColor[2])})`;
            content.style.backgroundColor = `rgba(${Math.round(currentStyles.bgColor[0])}, ${Math.round(currentStyles.bgColor[1])}, ${Math.round(currentStyles.bgColor[2])}, ${currentStyles.bgColor[3].toFixed(2)})`;
            content.style.fontSize = `${currentStyles.fontSize.toFixed(1)}px`;

            // Check if all transitions are complete
            if (
                currentStyles.textColor.every(
                    (value, index) => value === targetStyles.textColor[index]
                ) &&
                    currentStyles.bgColor.every(
                        (value, index) => value === targetStyles.bgColor[index]
                    ) &&
                    currentStyles.fontSize === targetStyles.fontSize
            ) {
                clearInterval(interval); // Stop the loop
            }
        }, 100); // Interval duration
    }, 800); // Delay before transition starts
});
